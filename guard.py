import pygame
import entity, utils, player, traps, inventory
import random
from enum import Enum

class Guard_types(Enum):
    patroling=0
    lazy=1
    crossbow=2

class Guard(entity.Entity):
    def __init__(self, x, y, color=None):
        super(Guard, self).__init__(x, y)
        self.char = 'G'
        self.solid = True
        self.hitting = True
        self.color=(pygame.Color(120,30,40) if color is None else color)
        self.type=Guard_types.patroling
        self.route=[] #patrol route
        self.next_route_stop=0
        self.looking_around_ticks=0
        self.viewcone={}
        self.look_direction=(0,-1)
        self.path=[]
        self.last_player_pos=False
        self.angry=False
    def __regen_viewcone(self,room):
        def is_visible(x,y):
            return not room.is_opaque(x,y)
        def add_dir_tile(direction):
            x,y=direction
            x+=self.x
            y+=self.y
            if is_visible(x,y):
                self.viewcone[(x,y)]=True
        self.viewcone=utils.viewcone(self.x,self.y,is_visible,6,self.look_direction)
        #add periferal vision
        dir_index=utils.offsets4.index(self.look_direction)
        dir_index_next=(dir_index+1)%len(utils.offsets4)
        add_dir_tile((utils.offsets4[dir_index_next]))
        dir_index_prev=dir_index-1
        if dir_index_prev<0:
            dir_index_prev=len(utils.offsets4)-1
        add_dir_tile((utils.offsets4[dir_index_prev]))
    def __draw_viewcone(self,room):
        for x,y in self.viewcone.keys():
            if self.angry:
                room.tint.bg[x][y]=pygame.Color(200,0,0)
            else:
                room.tint.bg[x][y]=pygame.Color(208,100,0)
            room.tint.fade[x][y]=0.4
    def __see_player(self,room):
        if room.player.invisible:
            return False
        if (room.player.x,room.player.y) in self.viewcone:
            return True
    def __looking_around(self,room):
        if self.looking_around_ticks:
            self.looking_around_ticks-=1
            dir_index=utils.offsets4.index(self.look_direction)
            dir_index=(dir_index+1)%len(utils.offsets4)
            self.look_direction=utils.offsets4[dir_index]
            self.__regen_viewcone(room)
            return
    def __gen_route(self,room):
        number_of_points=3 #TODO: more random here?
        walkable=list(room.walkable.keys()) #TODO: better implementation?
        for x in range(1,number_of_points):
            self.route.append(random.choice(walkable))
    def __pathfind(self,room,pos):
        self.path=utils.bfs(self.x, self.y,lambda x,y: room.is_walkable(x,y), pos[0], pos[1])
    def __step(self,room):
        #path could have changed!!
        old_pos=self.path[-1]
        self.__pathfind(room,old_pos)
        #now we step
        if len(self.path)<=1:
            return False #failed to find path!
        x,y=self.path[1]
        #we throw away the old path anyways... so don't pop the element off
        self.look_direction=(x-self.x,y-self.y)
        self.x=x
        self.y=y
        self.__regen_viewcone(room)
        self.__draw_viewcone(room)
        return True
    def hit(self, room, obj):
        if not (isinstance(obj, player.Player) or isinstance(obj, Guard)):
            self.remove=True
            room.get_floor(self.x,self.y).color=pygame.Color(255,0,0)

    def tick(self, room, actions):
        # few ideas:
        #   generate random patrol route
        #   lazy bastards (stand and turn?)
        mypos=(self.x,self.y)
        if self.__see_player(room):
            self.__pathfind(room,(room.player.x,room.player.y))
            self.last_player_pos=(room.player.x,room.player.y)
            self.angry=True

        #now we have a path so walk it.
        if len(self.path)>0:
            if self.__step(room):#if we can't step, we resort to other logic
                return

        if self.last_player_pos and self.last_player_pos==mypos:
            self.looking_around_ticks=4
            self.last_player_pos=False
            self.angry=False

        self.__draw_viewcone(room)
        #now rest of logic
        if self.type==Guard_types.lazy:
            self.looking_around_ticks=4
            self.__looking_around(room)
            return
        elif self.type==Guard_types.patroling:
            if len(self.route)==0: #gen route if we don't have one
                self.__gen_route(room)

            if self.looking_around_ticks>0: # we need to look around. Either on the end of patrol, or player position
                self.__looking_around(room)
                return
            elif self.route[self.next_route_stop]==mypos:
                self.looking_around_ticks=4
                self.next_route_stop=(self.next_route_stop+1)%len(self.route)
                self.__pathfind(room,self.route[self.next_route_stop])
                self.angry=False
            else:
                self.next_route_stop=random.randint(0,len(self.route)-1)
                self.__pathfind(room,self.route[self.next_route_stop])
                self.angry=False
        else:
            raise Exception("Wrong guard type, sorry!")

class Lazy_Guard(Guard):
    def __init__(self, x, y, color=None):
        super().__init__(x, y, color)
        self.type=Guard_types.lazy
            
class Crossbow_Guard(Guard):
    """A stationary guard armed with a crossbow. It can move sideways!"""
    def __init__(self, x, y, color=None):
        """Make a guard armed with an xbow."""
        super().__init__(x, y, color)
        self.solid = False
        self.hitting = False
        self.char = 'b'
        self.type=Guard_types.crossbow
        self.station = (x, y)
        self.pos_expire=0
        self.look_direction=random.choice(utils.offsets4) #spawn with random direction
        self.reload_time = 4
        self.reloading = self.reload_time
        self.shot = False
        self.pos_expire=-2
        self.drop = False
        self.queued = None

    def _regen_viewcone(self, room):
        self.viewcone=utils.viewcone(self.x,self.y,lambda x,y: not room.is_opaque(x,y),7,self.look_direction)

    def _is_walkable(self, x, y, room):
        if (x,y)==(room.player.x,room.player.y):
            return True
        return room.is_walkable(x,y)

    def _sign(self, x):
        if x==0: return 0
        if x>0: return 1
        return -1

    def __shoot(self, direction, room, actions):
        self.reloading = self.reload_time
        self.shot = True
        self.char = 'b'
        bolt=traps.Crossbow_Bolt(self.x, self.y, direction, 3)
        d=utils.offsets4[direction]
        if room.is_walkable(self.x+d[0]*3, self.y+d[1]*3): room.objects[(self.x+d[0]*3, self.y+d[1]*3)].append(bolt)
        bolt.tick(room, actions)
        bolt.ticked=True

    def hit(self, room, obj):
        super().hit(room, obj)
        if self.remove and not self.drop:
            inventory.gen_item(self.x, self.y, 'Crossbow bolt', room)
            self.drop = True

    def tick(self, room, actions):
        """Look for player and try to shoot him if seen."""
        direction=utils.offsets4.index(self.look_direction)
        #Determine player position
        if self._Guard__see_player(room):
            self.last_player_pos=(room.player.x,room.player.y)
            self.shot = False
            self.pos_expire=4
            self.queued = None
        else: self.pos_expire-=1
        
        if self.reloading:
            #Reload if not loaded
            self.reloading-=1
            if not self.reloading: self.char = 'B'
        elif self.pos_expire>0:
            dx=self.last_player_pos[0]-self.x
            dy=self.last_player_pos[1]-self.y
            close=((self._sign(dx),0) if abs(dx)>=abs(dy) else (0,self._sign(dy)))
            #If player location known, try to shoot him
            if (self.x==self.last_player_pos[0]) and (self.y==self.last_player_pos[1]) and not self.shot:
                #If player is in the same position as you, dive in a random direction and shoot at it
                valid_dirs=[]
                for dirs in range(4):
                    if self._is_walkable(self.x+utils.offsets4[dirs][0], self.y+utils.offsets4[dirs][1], room): valid_dirs.append(dirs)
                if len(valid_dirs)>0:
                    dive_dir = random.choice(valid_dirs)
                    dive = utils.offsets4[dive_dir]
                    self.x, self.y = self.x+dive[0],self.y+dive[1]
                    self.__shoot((direction+2)%4, room, actions)
            elif (self.x==self.last_player_pos[0] or self.y==self.last_player_pos[1]) and not self.shot:
                self.__shoot(utils.offsets4.index(close), room, actions)
            else:
                #Try to get into shooting position if cannot shoot straight
                #If that is an empty square, will shoot the same turn (to prevent stepping into a trap and shooting before dying to it. :)
                if min(abs(dy),abs(dx))<=3:
                    strafe=((self._sign(dx),0) if abs(dx)<abs(dy) else (0,self._sign(dy)))
                    if self._is_walkable(self.x+strafe[0]+close[0],self.y+strafe[1]+close[1], room):
                        if self._is_walkable(self.x+strafe[0],self.y+strafe[1], room):
                            self.x, self.y = self.x+strafe[0],self.y+strafe[1]
                            if (self.x==self.last_player_pos[0] or self.y==self.last_player_pos[1]) and not self.shot and len(room.objects[(self.x, self.y)])<2:
                                self.__shoot(utils.offsets4.index(close), room, actions)
                        else:
                            self.x, self.y = self.x+close[0],self.y+close[1]
                            if (self.x==self.last_player_pos[0] or self.y==self.last_player_pos[1]) and not self.shot and len(room.objects[(self.x, self.y)])<2:
                                self.__shoot(utils.offsets4.index(strafe), room, actions)
                    else:
                        self.x, self.y = self.x+close[0],self.y+close[1]
                    self._regen_viewcone(room)
                
        elif self.station != (self.x, self.y):
            #If lost player, try to return to station
            self._Guard__pathfind(room, (self.x, self.y))
            self._Guard__step(room)
            if self.pos_expire>-2: self.look_direction=utils.offsets4[direction]
            self._regen_viewcone(room)
        elif self.queued is not None:
            self.look_direction = utils.offsets4[self.queued]
            self._regen_viewcone(room)
            self.queued = None
        elif random.randint(0,2)!=0:
            #look around
            valid_dirs=[]
            for dirs in range(4):
                if self._is_walkable(self.x+utils.offsets4[dirs][0], self.y+utils.offsets4[dirs][1], room): valid_dirs.append(dirs)
            if len(valid_dirs)>1 and (direction in valid_dirs): valid_dirs.remove(direction)
            if len(valid_dirs)>1 and ((direction+2)%4 in valid_dirs): valid_dirs.remove((direction+2)%4)
            if len(valid_dirs)>0:
                new_dir=random.choice(valid_dirs)
                if new_dir%2==direction%2:
                    self.queued = new_dir
                    self.look_direction = utils.offsets4[(new_dir+random.choice((-1,1)))%4]
                else: self.look_direction = utils.offsets4[new_dir]
            self._regen_viewcone(room)
        #draw the viewcone
        self._Guard__draw_viewcone(room)
            

def generate(room,walkable,numberGuards=3, numberCrossbowGuards=1):
    #place the guards using new experimental method
    utils.place(room, walkable, Crossbow_Guard, numberCrossbowGuards, 5, 5, True)
    utils.place(room, walkable, Guard, numberGuards, 3, 4, False)
