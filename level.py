import random, copy
import utils
import entity
import critters
import inventory
import traps
import guard
import player
import tiers, level_settings
import effects
import pygame

structglyph = (' ','#','.','@') #nothing, wall, floor and player respectively
glyph_dict = {'#': entity.Wall,
              '.': entity.Floor,
              '@': player.Player,
              '>': player.RoomExit,
              'G': guard.Guard,
              'B': guard.Crossbow_Guard,
              'b': guard.Crossbow_Guard,
              'L': guard.Lazy_Guard,
              'p': traps.Pot,
              'P': traps.Pot,
              'O': traps.Boulder,
              '+': traps.Boulder_Trap,
              'X': traps.PlaceableTrap,
              's': critters.Spiderling,
              'c': critters.Kitten,
              '%': critters.Smoke,
              'm': critters.Smoke_Bug,
              'Q': critters.Power_Source,
              '$': critters.Power_Snake,
              'Y': critters.The_Tree,
              '!': inventory.Item,
              'y': inventory.Sapling,
              '|': inventory.Bolt,
              '—': inventory.Bolt
              }

class Level:
    def __init__(self, width, height, game_mode, tier, data=None):
        assert width > 0 and height > 0

        self.width = self.level_width = width
        self.height = self.level_height = height
        self.vertical_offset = self.horizontal_offset = 0
        self.tier = tier

        # Effect layer
        self.tint= effects.Effects(width, height)
        # True at (x, y) position means it is possible to walk on it.
        self.walkable = {}
        # Map from (x, y) pairs to lists of objects.
        self.objects = {}
        self.player = None
        
        if game_mode == utils.gamemodes[4]: gen=self._custom_level_gen(game_mode, tier, data['selected_levels'])
        else: gen=self._random_level_gen(game_mode, tier)
        
        if self.height < self.level_height or self.width < self.level_width:
            print("Level too large")
            self.player=None
        
    def _random_level_gen(self, game_mode, tier):
        """random level generation"""
        generator_settings=level_settings.get_level_settings(game_mode, tier)
        self.no_intersect=generator_settings['no_intersect']
        self.terminate_early=generator_settings['terminate_early']
        self.room_biggening=generator_settings['room_biggening']
        # Generate level.
        intersections=self.__generate_corridors(generator_settings['num_corridor'])
        if generator_settings['num_room_tiles']>0:
            self.__generate_rooms(list(intersections.keys()),generator_settings['num_room_tiles'])
        self.__put_walls()
        
        #create the player 
        px, py = self.width // 2, self.height // 2
        self.player = player.Player(px, py)

        counts = tiers.level_setup[game_mode](self.tier)
        #temporary permanent solution
        #currently generator settings overwrite counts, should probably be the other way around
        if 'counts' in generator_settings: counts.update(generator_settings['counts'])

        critters.generate(self,self.walkable,
                          counts['number_spiders'],
                          counts['number_kittens'],
                          counts['number_smoke_bug'],
                          counts['number_power_source'],
                          counts['number_snake']
                          )
        traps.generateTraps(self, self.walkable,
                            counts['number_boulders'],
                            counts['number_pots']
                            )
        guard.generate(self, self.walkable,
                       counts['number_guards'],
                       counts['number_crossbow_guards']
                       )
        # Add items.
        inventory.generate(self, self.walkable,
                       counts['number_items']
                       )
        # Generate map exit condition.
        self.exit_difficulty = counts['exit_difficulty']
        if self.exit_difficulty is None: self.__place_exit()
        else: inventory.add_exit(self, counts['exit_difficulty'], counts['give_sapling'])

        # Add player object to grid.
        self.objects[(px, py)].append(self.player)
        
        return True
        
    def _custom_level_gen(self, game_mode, tier, level_file):
        """Load a level from file."""
        blank=True
        level=False
        status=True
        self.exit_difficulty=15 #default
        self.level_width=0
        level_store=[]
        with open(level_file) as level_file:            
            for line in level_file:
                if len(line)<=1:
                    if level:
                        level=False
                        tier-=1
                    if tier<0:
                        break
                    blank=True
                    continue
                if not level:
                    level=True
                    if tier==0 and line[:-1].isdigit():
                        self.exit_difficulty = int(line[:-1])
                        continue
                if tier==0:
                    l=line.rstrip(' \n')
                    level_store.append(l)
                    self.level_width=max(self.level_width,len(l))
        self.level_height=len(level_store)
        dx=(self.width-self.level_width)//2
        dy=(self.height-self.level_height)//2
        for j in range(len(level_store)):
            l=level_store[j]
            for i in range(len(l)):
                status=self.generate_entity_by_glyph(l[i],i+dx,j+dy) and status
        if tier>0 or (tier==0 and not level): return True
        if self.player is None: print ("No player placed in the level.")
        if not status: print("Not all objects recognized in level.")
        return status

    def generate_entity_by_glyph(self, glyph, x, y):
        """Generates an entity by the given glyph and places it on the grid."""
        assert isinstance(glyph, str) and len(glyph)==1
        #hardcoding empty space, floors and walls for convenience
        if glyph in structglyph[0]: return True
        if glyph in structglyph[1]:
            self.objects[(x,y)]=[(glyph_dict[glyph](x, y))]
            return True
        #everything else implies the place is passable
        self.walkable[(x,y)]=True
        if glyph in structglyph[2]:
            if (x,y) in self.objects: self.objects[(x,y)].append(glyph_dict[glyph](x, y))
            else: self.objects[(x,y)]=[(glyph_dict[glyph](x, y))]
            return True
        #and has a floor
        if (x,y) not in self.objects or len(self.objects[(x,y)])==0:
            self.objects[(x,y)]=[(glyph_dict[structglyph[2][0]](x, y))]
        if glyph in structglyph[3]:
            #check if we are not placing a second player
            if self.player is not None: return False
            #place the player and assign
            self.player=glyph_dict[glyph](x, y)
            self.objects[(x,y)].append(self.player)
            return True
        #check if glyph is in dictionary and place the item
        if glyph in glyph_dict:
            self.objects[(x,y)].append(glyph_dict[glyph](x, y))
            return True
        #numbers produce the corresponding items (unless overriden by dict)
        elif glyph.isdigit():
            if int(glyph)>=len(inventory.item_list): return False
            else: return inventory.gen_item(x, y, int(glyph), self)
        return False
            
    def __place_exit(self,iterations=500):
        """Tries placing exit a number of times. Picks longes way away one"""
        walkable=list(self.walkable.keys())
        best_choice=False
        best_distance=0
        for i in range(iterations):
            coord=random.choice(walkable)
            
            d=len(utils.bfs(coord[0],coord[1],lambda x,y: self.is_walkable(x,y),self.player.x,self.player.y))
            if best_distance<d:
                best_distance=d
                best_choice=coord
        if best_choice:
            self.objects[coord].append(player.RoomExit(best_choice[0],best_choice[1]))

    def __generate_randomwalk(self, n_steps=300):
        """Generates a random walkable space by taking n steps in random
        directions."""
        x, y = self.width // 2, self.height // 2
        self.walkable[(x, y)] = True
        steps = 0
        while steps < n_steps:
            dx, dy = utils.offsets4[random.randint(0, 3)]
            if (0 < x+dx < self.width-1) and (0 < y+dy < self.height-1):
                x, y = x+dx, y+dy
                if (x, y) not in self.walkable:
                    self.walkable[(x, y)] = True
                    steps = steps+1
    def __walkable_rhombus(self,cx,cy,r):
        """Sets all tiles in rhombus r around cx,cy as walkable"""
        count=0
        for x in range(-r,+r):
                for y in range(-r,+r):
                    in_range=abs(x)+abs(y)<r
                    tx,ty=cx+x,cy+y
                    if (0 < tx < self.width-1) and (0 < ty < self.height-1) and in_range:
                        if (tx,ty) not in self.walkable:
                            self.walkable[(tx, ty)] = True
                            count=count+1
        return count
    def __walkable_circle(self,cx,cy,r):
        """Sets all tiles in radius r around cx,cy as walkable"""
        r_sqr=r*r
        count=0
        for x in range(-r,+r):
                for y in range(-r,+r):
                    in_range=((x*x)+(y*y))<r_sqr
                    tx,ty=cx+x,cy+y
                    if (0 < tx < self.width-1) and (0 < ty < self.height-1) and in_range:
                        if (tx,ty) not in self.walkable:
                            self.walkable[(cx+x, cy+y)] = True
                            count=count+1
        return count
    def __generate_blobs(self,n_blobs=10):
        """Generate some walkable blobs"""
        current=0
        while current<n_blobs:
            current=current+1
            cx,cy=random.randint(1,self.width-1),random.randint(1,self.height-1)
            r=random.randint(3,8)
            print(r)
            r_sqr=r*r
            self.__walkable_circle(cx,cy,r)
    def __generate_corridors(self,n_steps=200):
        intersections={}
        steps=0
        x, y=self.width // 2, self.height // 2
        self.walkable[(x, y)] = True
        steps = 0
        stuck = True
        dx,dy=0,0
        chance_terminate=self.terminate_early
        while steps < n_steps:
            if stuck:
                dx, dy = utils.offsets4[random.randint(0, 3)]
                x,y=random.choice(list(self.walkable.keys()))
                stuck=False
            if (0 < x+dx < self.width-1) and (0 < y+dy < self.height-1):
                x, y = x+dx, y+dy
                if (x, y) not in self.walkable:
                    if self.no_intersect: #don't want intersections, so look one step further
                        if (x+dx,y+dy) not in self.walkable:
                            self.walkable[(x, y)] = True
                            steps+=1
                        else:
                            stuck=True
                    else:
                        self.walkable[(x, y)] = True
                        steps = steps+1
                else:
                    intersections[(x,y)] = True
                if random.random()<chance_terminate:
                    stuck=True
            else:
                stuck=True
        return intersections
    def __generate_rooms(self,centers,n_steps=100):
        steps = 0
        r=2
        iteration=0
        iter_to_bigger=self.room_biggening
        while steps < n_steps:
            x,y=random.choice(centers)
            added_tiles=self.__walkable_rhombus(x,y,r)
            steps=steps+added_tiles
            iteration=iteration+1
            if iteration>iter_to_bigger:
                iteration=0
                r=r+1
    def __put_walls(self):
        """Puts floors and walls around walkable tiles."""
        def is_walkable(x, y):
            if (0 <= x < self.width) and (0 <= y < self.height):
                return (x, y) in self.walkable and self.walkable[(x, y)] == True
            return False

        def visit(x, y):
            self.objects[(x, y)] = [entity.Floor(x, y)]
            for dx, dy in utils.offsets8:
                nx, ny = x+dx, y+dy
                if not is_walkable(nx, ny):
                    self.objects[(nx, ny)] = [entity.Wall(nx, ny)]

        x, y = self.width // 2, self.height // 2
        utils.dfs(x, y, is_walkable, visit)

    def is_walkable(self, x, y):
        if (x, y) in self.walkable:
            for o in self.objects[(x, y)]:
                if o.solid:
                    return False
            return True
        return False

    def is_opaque(self, x, y):
        if (x, y) not in self.walkable:
            return True
        for o in self.objects[(x, y)]:
                if o.opaque:
                    return True
        return False

    def get_floor(self, x,y):
        for obj in self.objects[(x,y)]:
            if isinstance(obj,entity.Floor): return obj
        raise Exception("Floor missing")

    def render(self, console, dx=0, dy=0):
        """Renders level to a textmode console."""
        old_color = console.selected_fg
        for x, y in self.objects.keys():
            tile_objects = self.objects[(x, y)]
            if tile_objects != None and len(tile_objects) > 0:
                console.selected_fg = tile_objects[-1].color
                console.print(x+dx, y+dy+1, tile_objects[-1].char) #y+dy+1 to accommodate the message bar at the top
        console.selected_fg = old_color
        # Displays level tier.
        #console.print(console.width - len(str(self.tier)) -19 , console.height-1, "Floors completed: ", pygame.Color(70,70,70))
        console.print(console.width - len(str(self.tier)) -1 , console.height-1, str(self.tier))

    def tick(self, actions):
        for pos in self.objects.keys():
            for o in self.objects[pos]:
                o.ticked = False
        
        #Objects will leave their positions pseudo-simultaneously,
        #reducing object movement dependency on the underlying memory structures.
        #Objects still enter grid positions non-simultaneously to prevent object collisions.
        #As the position stored on the object is updated immediately, shouldn't mess up any pathfinding 
        def dereference_positions():
            for pos, obj in self.deferred_dereferencing:
                self.objects[pos].remove(obj)
        
        #Storage for deferred dereferencing:
        self.deferred_dereferencing=[]
        
        def tick_and_update_pos(obj, actions):
            if obj.ticked == True or obj.remove == True:
                return False
            x, y = obj.x, obj.y
            res = obj.tick(self, actions)
            obj.ticked = True
            nx, ny = obj.x, obj.y
            if x != nx or y != ny:
                self.deferred_dereferencing.append(((x, y),obj)) #remove objects only after they have moved
                self.objects[(nx, ny)].append(obj)
            return res

        # Try to update player first, everything else if player did
        # something.
        if tick_and_update_pos(self.player, actions):
            self.tint.update() #fadeout tint before guard actions if ticking them
            for pos in self.objects.keys():
                for o in self.objects[pos]:
                    if o == self.player:
                        continue
                    tick_and_update_pos(o, [])
            dereference_positions()

            # Call 'hit' for all overlapping objects.
            for pos in self.objects.keys():
                objs = self.objects[pos]
                n = len(objs)
                if n >= 2:
                    for i in range(0, n):
                        for j in range(i+1, n):
                            if objs[j].hitting:
                                objs[i].hit(self, objs[j])
                            if objs[i].hitting:
                                objs[j].hit(self, objs[i])

            # Remove dead objects.
            for pos in self.objects.keys():
                self.objects[pos] = list(filter(lambda x: x.remove == False, self.objects[pos]))

            return True
        return False

