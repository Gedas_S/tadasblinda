import pygame
import entity, utils, traps, player
import random

class Bug(entity.Entity):
    """An abstract class which the Kitten hunts and generally includes weak critters which die from anything."""
    pass

class Spiderling(Bug):
    """A little harmless spider (unless you have arachnophobia :P), leaves blood splatter on the ground"""
    def __init__(self, x, y, color=None):
        """Make a spider."""
        super(Spiderling, self).__init__(x, y)
        self.char = 's'
        self.solid = False
        self.hitting = False
        #if color not defined, generate a random one
        if color is not None: self.color=color
        else:
            self.color=pygame.Color(random.randint(0,255),
                                    random.randint(0,255),
                                    random.randint(0,255))

    def hit(self, room, obj):
        """Leave a blood splat and die."""
        #call the floor and tell it to change color to spider color
        #aka SPLASH
        room.get_floor(self.x,self.y).color=self.color
        #remove the spider
        self.remove = True
        if isinstance(obj, player.Player):
            obj.inventory.set_message("It was a small spider.")

    def tick(self, room, actions):
        """Wander randomly in one of 8 direction, stay put if it's unwalkable."""
        move=utils.offsets8[random.randint(0,7)]
        if room.is_walkable(self.x+move[0],self.y+move[1]):
            self.x+=move[0]
            self.y+=move[1]

class Kitten(entity.Entity):
    """A cat which wanders around, hunting bugs and triggering traps."""
    def __init__(self, x, y, color=None):
        """Make a cat."""
        super().__init__(x, y)
        self.char = 'c'
        self.solid = False
        self.hitting = False
        self.direction = random.choice(range(4))
        #if color not defined, generate a random one
        if color is not None: self.color=color
        else:
            a=random.randint(0,255)
            self.color=pygame.Color(a,a,a)

    def tick(self, room, actions):
        """Move around, chasing bugs if any found."""
        chases=[]
        for i in range(3):
            for obj in room.objects[(self.x,self.y)]:
                if isinstance(obj,Bug):
                    chases.append(i)
        if len(chases)>0:
            self.direction = random.choice(chases)
            self.x+=utils.offsets4[self.direction][0]
            self.y+=utils.offsets4[self.direction][1]
        elif not random.randint(0,2)==0:
            valid_dirs=[]
            for dirs in range(4):
                if room.is_walkable(self.x+utils.offsets4[dirs][0], self.y+utils.offsets4[dirs][1]): valid_dirs.append(dirs)
            if len(valid_dirs)>1 and ((self.direction+2)%4 in valid_dirs): valid_dirs.remove((self.direction+2)%4)
            if len(valid_dirs)>0:
                self.direction = random.choice(valid_dirs)
                self.x+=utils.offsets4[self.direction][0]
                self.y+=utils.offsets4[self.direction][1]
        for obj in room.objects[(self.x,self.y)]:
            if isinstance(obj,traps.Trap):
                obj.hit(room, self)
            if isinstance(obj,Spiderling):
                obj.remove=True #if it's a spider, eat it without splash
            elif isinstance(obj,Bug):
                obj.hit(room,self)

    def hit(self, room, obj):
        """Die if hit by a boulder."""
        if isinstance(obj,traps.Boulder):
            #call the floor and tell it to change color to red
            room.get_floor(self.x,self.y).color=pygame.Color(255,0,0)
            #remove the kitten
            self.remove = True
        if isinstance(obj, player.Player):
            obj.inventory.set_message("It's a small cat.")

class Smoke(entity.Entity):
    """Opaque non-interactive smoke which decays each turn with 10% chance."""
    def __init__(self, x, y, color=None):
        """Make some smoke."""
        super().__init__(x, y)
        self.char = '%'
        self.solid = False
        self.hitting = False
        self.opaque = True
        self.duration = 8
        #if color not defined, generate a random one
        if color is not None: self.color=color
        else:
            a=random.randint(120,255)
            self.color=pygame.Color(a,a,a)

    def hit(self, room, obj):
        """Disregard hits."""
        pass

    def tick(self, room, action):
        """Proc disappearance chance."""
        if self.duration == False:
            # after base time 10% chance to disappear each turn
            if random.randint(0,12)==0: self.remove=True
        elif self.duration == 0:
            self.duration = False
        else:
            self.duration -= 1       

class Smoke_Bug(Bug):
    """A bug, which, when killed, emits smoke."""
    def __init__(self, x, y, color=None):
        """Create a smoke bug."""
        super().__init__(x, y)
        self.char = 'm'
        self.solid = False
        self.hitting = False
        #if color not defined, generate a random one
        if color is not None: self.color=color
        else:
            a=random.randint(120,255)
            self.color=pygame.Color(a,a,a)

    def hit(self, room, obj):
        """Die, leaving behind a cloud of smoke."""
        #emit smoke when killed
        new_smoke=Smoke(self.x, self.y, self.color)
        if not self.remove: room.objects[(self.x,self.y)].append(new_smoke)
        new_smoke.ticked=True
        #remove the bug
        self.remove = True
        if isinstance(obj, player.Player):
            obj.inventory.set_message("This bug left a cloud of smoke when squashed.")

    def tick(self, room, actions):
        """Wander around, like a spider."""
        if room.player.x==self.x and room.player.y==self.y: return #caught!
        #just wander, no retries
        move=utils.offsets8[random.randint(0,7)]
        if room.is_walkable(self.x+move[0],self.y+move[1]):
            self.x+=move[0]
            self.y+=move[1]

class Power_Source(entity.Entity):
    """Some weird intangible green energy thing which charges the player with green energy which is used to charge the end tree."""
    def __init__(self, x, y):
        """Make a power source."""
        super().__init__(x, y)
        self.char='Q'
        self.color=pygame.Color(55,155,55)
        self.solid=False
        self.hitting=False
        self.opaque=True #the energy gets in the way and blocks sight, just for fun

    def hit(self, room, obj):
        """If a player with no charge enters it, disappear and charge the player."""
        if isinstance(obj, player.Player) and obj.power_charge==0:
            obj.power_charge=1
            obj.color=pygame.Color(55,155,55)
            self.remove=True
            obj.inventory.set_message("Strange power courses through you.")

class Power_Snake(Bug):
    """A bug which is a power source, but moving, and can be splashed by things."""
    def __init__(self, x, y):
        """Make a power snake."""
        super().__init__(x, y)
        self.char = '$'
        self.solid = False
        self.hitting = False
        self.color=pygame.Color(55,155,55)
        self.direction = random.choice(range(4))

    def tick(self, room, actions):
        """Wander around all the time."""
        valid_dirs=[]
        for dirs in range(4):
            if room.is_walkable(self.x+utils.offsets4[dirs][0], self.y+utils.offsets4[dirs][1]): valid_dirs.append(dirs)
        if len(valid_dirs)>1 and ((self.direction+2)%4 in valid_dirs): valid_dirs.remove((self.direction+2)%4)
        if len(valid_dirs)>0:
            self.direction = random.choice(valid_dirs)
            self.x+=utils.offsets4[self.direction][0]
            self.y+=utils.offsets4[self.direction][1]

    def hit(self, room, obj):
        """If hit by a player, charge him, else die."""
        if isinstance(obj, player.Player):
            obj.power_charge+=1
            obj.color=pygame.Color(55,155,55)
            obj.inventory.set_message("Strange power courses through you.")
        else:
            room.get_floor(self.x,self.y).color=pygame.Color(255,0,0)
        self.remove=True

class The_Tree(entity.Entity):
    """A tree which must be charged with energy to grow. If fully grown, will provide the level exit."""
    def __init__(self, x, y, progress_required=None):
        """Make a tree with zero progress."""
        super().__init__(x, y)
        self.char='Y'
        self.solid = False
        self.hitting = False
        self.color = pygame.Color(0,105,0)
        self.progress = 0
        self.progress_required = progress_required
        self.power = 0
        self.ticked = False

    def hit(self, room, obj):
        """Take power from player and lose progress if run over by a boulder."""
        #if its the player, take charge from it
        if isinstance(obj, player.Player):
            if obj.power_charge>0:
                self.power+=obj.power_charge*5
                obj.power_charge=0
                obj.color=pygame.Color(255,255,255)
                obj.inventory.set_message("The tree absorbs the power in you and starts growing.")
            if self.progress >= self.progress_required:
                #declare victory
                obj.victory=True
                obj.remove=True
        #if it's a boulder, lose some progress
        elif isinstance(obj, traps.Boulder):
            self.progress=max(self.progress-5,0)

    def tick(self, room, actions):
        """If charged, proc grow chance and change color according to progress."""
        if self.progress_required  is None: self.progress_required = room.exit_difficulty
        #50% chance to progress if it has power
        if self.power>0 and self.progress<self.progress_required and (random.randint(0,1)==1):
            self.progress+=1
            self.power-=1
        #set color accordingly
        self.color=pygame.Color(0,self.progress*10+105,min(10+self.power*40,255))
        if self.progress>=self.progress_required: self.color=pygame.Color(255,127,255)
        
def generate(room,walkable,
             numberSpiders=6, numberKittens=1, number_smoke_bug=3,
             number_power_source=5, number_snake=1):
    """Generate critters in the specified amounts."""
    utils.place(room, walkable, Spiderling, numberSpiders, 3)
    utils.place(room, walkable, Kitten, numberKittens, 4)
    utils.place(room, walkable, Smoke_Bug, number_smoke_bug, 3)
    utils.place(room, walkable, Power_Source, number_power_source, 1)
    utils.place(room, walkable, Power_Snake, number_snake, 2)

