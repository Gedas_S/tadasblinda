import pygame
import utils

class Textmode:
    def __init__(self, width, height, fontsize):
        assert width > 0 and height > 0 and fontsize > 0

        self.width = width
        self.height = height

        # 2d lists of characters, their backgound and foreground colors.
        self.chars = utils.list2d(width, height, ' ')
        self.bg = utils.list2d(width, height, pygame.Color(0, 0, 0))
        self.fg = utils.list2d(width, height, pygame.Color(255, 255, 255))

        # Newly printed chars will use these fg/bg colors.
        self.selected_bg = pygame.Color(0, 0, 0)
        self.selected_fg = pygame.Color(255, 255, 255)

        # pygame internals - font object and glyph cache.
        # TODO(mitkus): Add comma separated font alternatives for Win and OS X.
        self.font = pygame.font.SysFont('dejavusansmono,consolas,courier', fontsize)
        self.font_width = self.font.size('a')[0]
        self.font_height = self.font.get_linesize()
        self.glyph_cache = {}

    def __get_glyph(self, char, fg, bg):
        """Returns surface of a specified character glyph."""
        assert len(char) == 1
        key = (char, bg.r, bg.g, bg.b, fg.r, fg.g, fg.b)
        if key not in self.glyph_cache:
            # Glyph not in cache, render.
            surf = self.font.render(char, True, fg, bg)
            self.glyph_cache[key] = surf
        return self.glyph_cache[key]

    def __str__(self):
        """Debug printout helper."""
        printout = ""
        for y in range(self.height):
            printout += ''.join([self.chars[x][y] for x in range(self.width)])
            printout += '\n'
        return printout

    def clear(self):
        """Clears screen to a currently selected bg color."""
        self.chars = utils.list2d(self.width, self.height, ' ')
        self.bg = utils.list2d(self.width, self.height, self.selected_bg)

    def print(self, x, y, text, fg=None, bg=None):
        """Prints specified text on screen."""
        assert x >= 0 and x < self.width
        assert y >= 0 and y < self.height

        for char in text:
            self.chars[x][y] = char
            self.fg[x][y] = (self.selected_fg if fg is None else fg)
            self.bg[x][y] = (self.selected_bg if bg is None else bg)
            x = x+1
            if x >= self.width:
                x, y = 0, y+1

    def set_color(self, x, y, count, fg=None, bg=None):
        """Sets colors of a 'count' chars starting from x, y position."""
        assert x >= 0 and x < self.width
        assert y >= 0 and y < self.height

        for _ in range(count):
            if fg != None:
                self.fg[x][y] = fg
            if bg != None:
                self.bg[x][y] = bg
            x = x+1
            if x >= self.width:
                x, y = 0, y+1


    def render(self, surface):
        """Renders textmode screen to a specified surface."""
        fw, fh = self.font_width, self.font_height
        for y in range(self.height):
            for x in range(self.width):
                rect = pygame.Rect(x*fw, y*fh, (x+1)*fw, (y+1)*fh)
                glyph = self.__get_glyph(self.chars[x][y], self.fg[x][y],
                        self.bg[x][y])
                surface.blit(glyph, rect)
