import pygame
from pygame.locals import *
import textmode, utils
import pickle, sys
from os import listdir
from os.path import isfile

selected_item_bg = pygame.Color(208,100,0)
help_bg = pygame.Color(70,70,70)
name_of_the_game = "TADAS BLINDA"

right_help_width=20
left_help_width=20
help_right = ("Index:",
              "",
              "@  You",
              "#  Wall",
              ".  Floor",
              ">  Exit",
              "G  Melee guard",
              "B  Crossbow guard",
              "b  Reloading guard",
              "O  Boulder",
              "X  Trap",
              "%  Smoke",
              "Y  Magic tree",
              "Q  Source of power",
              "c  Cat",
              "s  Spider",
              "m  Smoke bug",
              "$  Snake",
              "p  Pot (big one)",
              "+  Boulder trap",
              "y  Tree sapling",
              "!  Item",
              "|— Crossbow bolt"
              )
              
help_left = ("Move with arrows,",
             "wait with space,",
             "tab - inventory,",
             "F1 - this help.",
             "",
             "Trees lead to",
             "victory somehow.",
             "Through blood.",
             "",
             "Animals are",
             "mostly harmless,",
             "fear the guards.",
             "",
             "We would like to",
             "assure you, this",
             "game contains no",
             "bees, or anything",
             "of the sort.",
             "",
             "Or just play the",
             "classic campaign",
             "for a simpler,",
             "cleaner roguelike",
             "experience."
             )

def show_menu(console, screen, selected_item):
    """Produce the main menu, which includes gamemode selection and highscores, and make it controllable."""
    #load current highscores
    highscores=load_scores()
    #define css
    menu_h=int(console.height*0.47)
    menu_hs=0.65
    menu_gm=0.4
    cont=True
    info=False
    if selected_item is None:
        cont=False
        selected_item=1
    selected_item = (selected_item)%(len(utils.gamemodes)+2)
    #enter control loop
    clockmaster=pygame.time.Clock()
    while True:
        #clear the screen and print everything
        console.clear()
        _menu_print(console, name_of_the_game, 0.5, 0.24)
        _menu_print(console, "Press TAB or F1 to toggle information", 0.5, 0.93)
        _menu_print(console, "Highscore", menu_hs, menu_h-1)
        _menu_print(console, "Select game mode:", menu_gm, menu_h-1)
        for x in range(len(utils.gamemodes)):
            if 4>x>0: _menu_print(console, str(highscores[utils.gamemodes[x]] if utils.gamemodes[x] in highscores else 0), menu_hs, menu_h+1+x)
            _menu_print(console, utils.gamemodes[x], menu_gm, menu_h+1+x, bg=(selected_item_bg if selected_item==x else None))
        if cont: _menu_print(console, 'Continue', menu_gm, menu_h+2+len(utils.gamemodes), bg=(selected_item_bg if selected_item==len(utils.gamemodes) else None))
        _menu_print(console, 'Quit', menu_gm, menu_h+3+len(utils.gamemodes), bg=(selected_item_bg if selected_item==len(utils.gamemodes)+1 else None))
        screen.fill(pygame.Color(0,0,0))
        if info: show_help(console, screen)
        console.render(screen)
        pygame.display.flip()
        
        in_menu=True
        while in_menu:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return None
                elif event.type == KEYDOWN:
                    mods = pygame.key.get_mods()
                    if (event.key == K_F4 and (mods & KMOD_ALT)) or (event.key == K_q and (mods & KMOD_CTRL)):
                        return None
                    if event.key == K_ESCAPE:
                        if not cont: return None
                        else: return -2
                    elif event.key in (K_KP_ENTER, K_RETURN, K_SPACE):
                        if utils.gamemodes.index("Custom levels") == selected_item:
                            selected_level=_custom_level_selection(console, screen, info)
                            if selected_level is None: return None
                            if selected_level: return (selected_item, {"selected_levels": selected_level})
                            in_menu=False
                        elif selected_item<len(utils.gamemodes): return selected_item
                        elif selected_item==len(utils.gamemodes): return -2
                        else: return None
                    elif event.key == K_DOWN:
                        selected_item = (selected_item+1)%(len(utils.gamemodes)+2)
                        if selected_item==len(utils.gamemodes) and not cont: selected_item+=1
                        in_menu=False
                    elif event.key == K_UP:
                        selected_item = (selected_item-1)%(len(utils.gamemodes)+2)
                        if selected_item==len(utils.gamemodes) and not cont: selected_item-=1
                        in_menu=False
                    elif event.key == K_TAB or event.key == K_F1:
                        info = not info
                        in_menu=False
            clockmaster.tick(30)

def _custom_level_selection(console, screen, info, folder="levels"):
    """Select a custom level."""
    selected_item = 0
    top_item = 0
    page_height = console.height - 6
    level_hs=0.65
    level_fi=0.4
    #load things in directory
    item_list=listdir(folder)
    depth=0
    highscores=load_scores()
    #enter control loop
    clockmaster=pygame.time.Clock()
    while True:
        #clear the screen and print everything
        console.clear()
        _menu_print(console, "Select level", level_fi, 1)
        _menu_print(console, "Highscore", level_hs, 1)
        _menu_print(console, 'Back', level_fi, min(console.height-2, len(item_list)+4), bg=(selected_item_bg if selected_item==len(item_list) else None))
        for x in range(top_item,min(len(item_list),top_item+page_height-1)):
            _menu_print(console, item_list[x], level_fi, x+3, bg=(selected_item_bg if selected_item==x else None))
            if (folder+'/'+item_list[x]) in highscores:
                f=highscores[folder+'/'+item_list[x]]
                _menu_print(console, str(round(f)), level_hs, x+3, color=(None if isinstance(f,int) else pygame.Color(120,255,120)))
        screen.fill(pygame.Color(0,0,0))
        if info: show_help(console, screen)
        console.render(screen)
        pygame.display.flip()
        
        in_menu=True
        while in_menu:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return None
                elif event.type == KEYDOWN:
                    mods = pygame.key.get_mods()
                    if (event.key == K_F4 and (mods & KMOD_ALT)) or (event.key == K_q and (mods & KMOD_CTRL)):
                        return None
                    if event.key == K_ESCAPE:
                        return False
                    elif event.key in (K_KP_ENTER, K_RETURN, K_SPACE):
                        if selected_item == len(item_list):
                            if depth==0: return False
                            else:
                                depth-=1
                                folder=folder.rsplit('/',1)[0]
                                selected_item = 0
                                top_item = 0
                                item_list=listdir(folder)
                                in_menu=False
                        elif isfile(folder+'/'+item_list[selected_item]):
                            return folder+'/'+item_list[selected_item]
                        else:
                            depth+=1
                            folder=folder+'/'+item_list[selected_item]
                            selected_item = 0
                            top_item = 0
                            item_list=listdir(folder)
                            in_menu=False
                    elif event.key == K_DOWN:
                        selected_item = (selected_item+1)%(len(item_list)+1)
                        in_menu=False
                    elif event.key == K_UP:
                        selected_item = (selected_item-1)%(len(item_list)+1)
                        in_menu=False
                    elif event.key == K_PAGEUP:
                        selected_item = max(selected_item-page_len,0)
                        top_item = max(top_item-page_len,0)
                        in_menu=False
                    elif event.key == K_PAGEDOWN:
                        selected_item = min(selected_item+page_len,len(item_list))
                        top_item = min(top_item+page_len,len(item_list)-page_len)
                        in_menu=False
                    elif event.key == K_TAB or event.key == K_F1:
                        info = not info
                        in_menu=False
            clockmaster.tick(30)
            
def show_help(console, screen, clear=False):
    """Render help on the screen"""
    if clear: #if rendering on top of the game, things see through
        for i in range(console.height):
            _menu_print(console, " "*left_help_width, 0, i, False, bg=help_bg)
            _menu_print(console, " "*right_help_width, console.width-right_help_width, i, False, bg=help_bg)
    for i in range(console.height):
        console.set_color(0,i,left_help_width,bg=help_bg)
        console.set_color(console.width-right_help_width,i,right_help_width,bg=help_bg)
    help_surface_l=pygame.Surface((left_help_width*console.font_width,console.height*console.font_height))
    help_surface_r=pygame.Surface((right_help_width*console.font_width,console.height*console.font_height))
    help_surface_l.fill(help_bg)
    help_surface_r.fill(help_bg)
    screen.blit(help_surface_l, (0,0))
    screen.blit(help_surface_r, ((console.width-right_help_width)*console.font_width,0))
    for i in range(len(help_left)):
        _menu_print(console, help_left[i], 1, i+1, False, bg=help_bg)
    for i in range(len(help_right)):
        _menu_print(console, help_right[i], console.width-right_help_width+1, i+1, False, bg=help_bg)
        
def _menu_print(console, text, x, y, center=True, color=None, bg=None):
    console.print((int(console.width*x) if 0<x<1 else x)-(len(text)//2 if center else 0), (int(console.height*y) if 0<y<1 else y), text, color, bg)
        
def save_score(gamemode, score, overwrite=False, name='highscores.dat'):
    """Save the score of specified gamemode into score file and return if the score was written.
    If overwrite=False (default), only writes if the score is higher than current."""
    highscores=load_scores(name)
    #I assume tutorial will proceed into campaign, so including those scores as well
    if gamemode=="Tutorial":
        gamemode="Campaign"
        score=max(score-10,0)
    if gamemode=="Custom levels": return False
    if (gamemode not in highscores) or overwrite or highscores[gamemode] < score or (isinstance(score,float) and highscores[gamemode] == score): #float means completed level
        highscores[gamemode] = score
        with open(name,'wb') as score_save:
            pickle.dump(highscores,score_save)
        return True
    else: return False
    
def load_scores(name='highscores.dat'):
    """Load highscores from set file returning them as dict."""
    #load highscores
    try:
        with open(name,'rb') as score_save:
            highscores=pickle.load(score_save)
    #make a file if not found
    except (FileNotFoundError,ValueError):
        highscores=dict()
        with open(name,'wb') as score_save:
            pickle.dump(highscores,score_save)
        if sys.exc_info()[0]==FileNotFoundError: print('No highscore file found, creating new file.')
        if sys.exc_info()[0]==ValueError: print('Highscore data corrupt, creating new file.')
    return highscores
