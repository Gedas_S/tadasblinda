import pygame
import entity, utils, player, guard
import random
import effects2

class Trap(entity.Entity):
    """Trap class for general object classification"""
    def __init__(self, x, y):
        super().__init__(x,y)
        self.hitting = False
        self.solid = False

class Boulder(entity.Wall):
    """a boulder which rolls in a straight line and crushes anything in its path"""
    def __init__(self, x, y, direction=None, integrity=7):
        """direction as defined in offset4, integrity is number of hits to survive"""
        super().__init__(x,y)
        self.solid = True
        self.hitting = True
        self.char = 'O'
        if direction is None: direction=random.randint(0,3)
        self.direction=direction
        self.integrity=integrity
        self.ticked=True

    def hit(self, room, obj):
        """Remove integrity when hit."""
        self.integrity-=1
        if self.integrity<=0: self.remove=True

    def __is_wall(self, x,y,room):
        for obj in room.objects[(x,y)]:
            if isinstance(obj,entity.Wall): return True
        return False

    def tick(self, room, actions):
        """Keep rolling, change direction if wall encountered."""
        #check if it hit a wall, if yes, change direction and reduce integrity
        if self.__is_wall(self.x+utils.offsets4[self.direction][0], self.y+utils.offsets4[self.direction][1],room):
            ways=(self.__is_wall(self.x+utils.offsets4[self.direction-1][0], self.y+utils.offsets4[self.direction-1][1],room),
                  self.__is_wall(self.x+utils.offsets4[self.direction-3][0], self.y+utils.offsets4[self.direction-3][1],room))
            if all(ways): self.direction+=2
            elif not any(ways): self.direction-=random.choice((1,3))
            elif not ways[0]: self.direction+=3
            elif not ways[1]: self.direction+=1
            else: self.remove=True
            self.direction=self.direction%4
            self.integrity-=2
            if self.integrity<=0: self.remove=True
        self.x, self.y = (self.x+utils.offsets4[self.direction][0], self.y+utils.offsets4[self.direction][1])

class Boulder_Trap(Trap):
    """A trap which releases a boulder rolling."""
    def __init__(self, x, y, direction=None):
        """Direction is which way the boulder will roll, random if not specified"""
        super().__init__(x,y)
        self.char = '+'
        self.color = pygame.Color(123,123,123)
        if direction is None: self.direction=random.randint(0,3)
        else: self.direction=self.direction
        self.is_active = False

    def tick(self, room, actions):
        """Reduce delay if active, pass otherwise."""
        #activate if active
        if self.is_active:
            self.delay-=1
            if self.delay==0:
                room.objects[(self.x,self.y)].append(Boulder(self.x, self.y, self.direction, 7))
                self.remove=True

    def hit(self, room, obj):
        """Activate the trap."""
        if not self.is_active:
            #activate in a few turns
            self.is_active=True
            self.delay=random.randint(1,3)
            if isinstance(obj, player.Player):
                obj.inventory.set_message("You hear a mechanical click.")

# Traps that are set up by a player
class PlaceableTrap(Trap):
    def __init__(self, x, y, hardness = 5):
        super(PlaceableTrap, self).__init__(x, y)
        self.char = 'X'
        self.solid = False
        self.hitting = False
        self.ticked = True
        self.hardness = min(hardness, 5)
        self.color = pygame.Color(self.hardness*50+5,0,0)

    def hit(self, room, obj):
        self.hardness -= 1
        if self.hardness == 0:
            self.remove = True
        self.color = pygame.Color(self.hardness*50+5,0,0)

    def tick(self, room, actions):
        self.hitting = True

#a pot for hiding in
class Pot(Trap):
    """A pot which can be crushed by boulders and the player can hide in them."""
    def __init__(self, x, y):
        """Generic constructor."""
        super().__init__(x, y)
        self.char = 'p'
        lightness = random.randint(30,60)
        self.color = pygame.Color(lightness*4,lightness*2,lightness)
        self.open = 0
        self.active = False
        self.hiding = False

    def hit(self, room, obj):
        """If boulder, get destroyed, if player standing for three turns, hide in the pot."""
        if isinstance(obj, Boulder):
            self.remove=True

    def tick(self, room, actions):
        """Remove the standing timer and player hiding if player left the pot."""
        if not (room.player.x==self.x and room.player.y==self.y):
            if self.active and self.hiding:
                self.hiding = False
                self.cover.remove=True
            self.active = False
        else:
            if self.open >2 and not self.hiding:
                self.hiding=True
                self.cover=Pot_Cover(self.x, self.y, self.color)
                room.objects[(self.x,self.y)].append(self.cover)
                room.player.inventory.set_message("Nobody will see you in this pot.")
            self.open +=1
            self.active = True
        if not self.active: self.open = 0

class Pot_Cover(Trap):
    """Helper class to spawn over the player when hidden in a pot."""
    def __init__(self, x, y, color):
        super().__init__(x, y)
        self.char = 'P'
        self.color = color
        
class Crossbow_Bolt(entity.Entity):
    """A crossbow bolt which flies straight and true and hits guards and walls."""
    def __init__(self, x, y, direction, speed=None):
        """Make a crossbow bolt going in the specific direction (at the specified speed)."""
        super().__init__(x, y)
        self.solid = False
        self.hitting = False
        self.direction = direction
        self.speed = (speed if speed is not None else random.choice((3,4,4,5)))
        self.char = ('|' if direction%2!=0 else '—')
        self.first_turn = True
        self.ticked = False
        
    def hit(self, room, obj):
        if (isinstance(obj,entity.Wall) or isinstance(obj,guard.Guard) or isinstance(obj,player.Player)):
            self.remove=True
            obj.hit(room, self)
        
    def tick(self, room, actions):
        """Fly forward and hit objects if any get in the way."""
        done = False
        for i in range((1 if self.first_turn else 0), self.speed+1):
            md=i
            x, y = self.x+i*utils.offsets4[self.direction][0], self.y+i*utils.offsets4[self.direction][1]
            for obj in room.objects[(x,y)]:
                if isinstance(obj,entity.Wall) or isinstance(obj,guard.Guard) or (isinstance(obj,player.Player) and obj.x==x and obj.y==y):
                    if i!=self.speed or isinstance(obj,entity.Wall):
                        self.remove=True
                        obj.hit(room, self)
                    done = True
                    if isinstance(obj,entity.Wall): md -=1
                    break
            if done: break
        #Fire the animation
        effects2.animator.animate_movement(self, self.direction, md, room, 20)
        self.x, self.y = x, y
        
def placePlaceableTrap(player, room):
    room.objects[(player.x),(player.y)].append(PlaceableTrap(player.x, player.y))

def generateTraps(room, walkable, numberBoulders=2, number_pots=3):
    """Generate the specified number of boulder traps and pots on walkable tiles."""
    utils.place(room, walkable, Boulder_Trap, numberBoulders, 5)
    utils.place(room, walkable, Pot, number_pots, 5)
