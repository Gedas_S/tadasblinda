import random
import pygame
import utils, effects2
import traps, critters
import entity

"""item list structure: name, description, activation message, color, (glyph), (additional message)"""
item_list = [
['Clear potion','A clear liquid inside transparent bottle.', 'You are almost invisible. For a while.', pygame.Color(200, 200, 200)], 
['Blue potion', 'Blue light blinks around in a bottle.', 'You teleported!', pygame.Color(75, 75, 255), '!', 'You did not teleport.'],
['Gray potion', 'A bottle with foggy liquid inside. You think you should not drink it.', 'As you open the bottle large amounts of fog pours out.', pygame.Color(100, 100, 100)], 
['Yellow potion', 'A bottle with tiny human inside.', 'Doppleganger created.', pygame.Color(200, 200, 0)],
['Placeable trap', 'You can place this trap and hope for a guard to step on it.', 'Trap set up. Do not get caught in it yourself.', pygame.Color(255, 0, 0)],
['Strange sapling', 'Is that blood on the roots?', 'Tree planted.', pygame.Color(55,255,55), 'y', 'Ground is too dry.'],
['Crossbow bolt', 'A harbinger of swift death.', 'Bolt shot.', pygame.Color(100,100,100), '|']
]

random_table = [0, 1, 2, 4]

class Item(entity.Entity):
    def __init__(self, x, y, n=None):
        super(Item, self).__init__(x, y)
        if n is None: n = random.choice(random_table)
        self.id = n
        self.name = item_list[n][0]
        self.description = item_list[n][1]
        self.function = item_list[n][2]
        self.color = item_list[n][3]
        self.char = ('!' if (len(item_list[n])<=4) else item_list[n][4])
        self.solid = False

    def hit(self, room, obj):
    	if obj == room.player:
            if len(obj.inventory.contents) < 9:
                self.remove = True
                obj.inventory.add(self)
            else:
                obj.inventory.set_message("Inventory full.")

class Sapling(Item):
    def __init__(self, x, y): super().__init__(x, y, 5)
class Bolt(Item):
    def __init__(self, x, y): super().__init__(x, y, 6)

def gen_random_item(room, i = -1):
	x, y = list(room.walkable)[random.randint(0, len(list(room.walkable))-1)]
	if i == -1:
        # choose random potion
        # if more items implemented add index to list
        # TODO implement item index 3
		i = random.choice(random_table)
	return gen_item(x, y, i, room)

def gen_item(x, y, item, room):
	if item in [i[0] for i in item_list]:
		item = [i[0] for i in item_list].index(item)
	elif not (isinstance(item, int) and 0 <= item < len(item_list)):
		return False
	room.objects[(x, y)].append(Item(x, y, item))
	return True
    
def generate(room, walkable, number_items=4):
    item_types = [{'n': random.choice(random_table)} for i in range(number_items)]
    utils.place(room, walkable, Item, number_items, 1, 1, pass_on=item_types)

def add_exit(room, difficulty, is_sapling):
    if is_sapling:
        utils.place(room, room.walkable, Item, 1, 1, 1, pass_on=[{'n':5}])
    else:
        utils.place(room, room.walkable, critters.The_Tree, 1, 1, 1, pass_on=[{'progress_required':difficulty}])
        
def blink_object(room, obj, dist, direction=None):
    """Move an object in a specified direction firing the blink effect"""
    if direction is None:
        effects2.animator.message_prompt("Choose a direction?")
        direction = utils.get_direction_key()
    if direction is None: return False
    effects2.animator.animate_movement(obj, direction, dist, room, 25, pygame.Color(100,100,255), 40)
    dx, dy = utils.offsets4[direction]
    obj.x, obj.y = obj.x+dx*dist, obj.y+dy*dist
    if not room.is_walkable(obj.x, obj.y):
        obj.hit(room, effects2.Blink_Effect())
    return True

class Inventory:
    def __init__(self):
        self.contents = []
        self.show = False
        self.message = ''
        self.message_display_duration = 0
        self.message_color = None

    def add(self, item):
        self.contents.append(item)
        self.set_message(item.description)
        return True

    def remove(self, item):
        c = self.contents
        if item in utils.item_letters and utils.item_letters.index(item) < len(c):
            item = c[utils.item_letters.index(item)]
        elif not item in c:
            return False
        c.remove(item)
        return item
        
    def get_by_letter(self, item):
        """Get item reference from inventory letter."""
        c = self.contents
        if item in utils.item_letters and utils.item_letters.index(item) < len(c):
            item = c[utils.item_letters.index(item)]
        elif not item in c:
            return None
        return item

    def set_message(self, message, duration = 3, color = None):
        self.message = message
        self.message_display_duration = duration
        self.message_color = color

    def toggle(self):
        self.show = not self.show

    def use_item(self, obj, item_id, room):
        item = self.get_by_letter(item_id)
        if item:
            if self.__item_effect(obj, item, room):
                self.remove(item_id)
                self.set_message(item.function)
                return True #will only return true if item used successfully
            elif len(item_list[item.id])>=6:
                self.set_message(item_list[item.id][5])
        return False
                
    def __item_effect(self, obj, item, room, direction=None):
        if item.name == item_list[0][0]:
            obj.invisible = 8 # n ticks of invisibility
        elif item.name == item_list[1][0]:
            return blink_object(room, obj, 5)
        elif item.name == item_list[2][0]:
            cloud_radius = 3
            spiders = (random.randint(0,42)==13)
            if spiders: self.set_message('As you open the bottle spiders pour out. Wait, what?')
            x, y = (obj.x, obj.y)
            cloud = []
            for i in range(1-cloud_radius, cloud_radius):
                for j in range(abs(i)-cloud_radius+1, cloud_radius-abs(i)):
                    cloud.append((x+i, y+j))
            for c in cloud:
                if c in room.objects.keys():
                    is_wall = any([isinstance(i, entity.Wall)
                       for i in room.objects[c]
                       ])
                    if not is_wall:
                        if spiders and (c[0]!=x or c[1]!=y): room.objects[c].append(critters.Spiderling(c[0], c[1]))
                        elif not spiders: room.objects[c].append(critters.Smoke(c[0], c[1], pygame.Color(100, 100, 100)))
        elif item.name == item_list[3][0]:
            pass # doppleganger, add to random item generation when implemented
        elif item.name == item_list[4][0]:
            traps.placePlaceableTrap(obj, room)
        elif item.name == 'Strange sapling':
            if room.get_floor(obj.x, obj.y).color==pygame.Color(40,40,40):
                self.set_message(item_list[5][5])
                return False
            else:
                room.objects[(obj.x, obj.y)].append(critters.The_Tree(obj.x, obj.y, room.exit_difficulty))
        elif item.name == 'Crossbow bolt':
            if direction is None:
                effects2.animator.message_prompt("Choose the direction?")
                direction = utils.get_direction_key()
            if direction is None: return False
            room.objects[(obj.x, obj.y)].append(traps.Crossbow_Bolt(obj.x, obj.y, direction))
        return True

    def render(self, console, dx=2, dy=2):
        if self.show:
            if self.contents:
                for j in range(len(self.contents)):
                    text = utils.item_letters[j] + ') ' + self.contents[j].name
                    console.print(dx, j + dy, text)
            else:
                text = 'Empty'
                console.print(dx, dy, text)
        if self.message and self.message_display_duration:
            # centering message
            start = console.width // 2 - len(self.message) // 2
            console.print(start, 0, self.message, self.message_color)
            self.message_display_duration -= 1
        #Add a bar at the bottom showing current items.
        for i in range(len(self.contents)):
            console.print((i*2)+1, console.height-1, self.contents[i].char, self.contents[i].color)
