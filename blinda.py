#!/usr/bin/env python3

import sys
import math
import pygame
import textmode
import level
import utils, menu
import inventory
import effects, effects2
from pygame.locals import *

width, height = 80, 25

def setup_game(gamemode, tier, level_file=None):
    lvl = level.Level(width, height, gamemode, tier, level_file)
    if lvl.player is None: return None
    # do one tick of "wait" action
    lvl.tick([utils.no_action])
    return lvl

def main():
    pygame.init()
    #+2 to add a row on top and on bottom for messages and status
    console = textmode.Textmode(width, height+2, 18)
    w, h = console.width * console.font_width, console.height * console.font_height

    screen = pygame.display.set_mode((w, h))
    pygame.display.set_caption('tadasblinda')

    console.selected_fg = pygame.Color(196, 196, 196)
    effects2.animator=effects2.Animator(console, screen, w, h)
    pygame.key.set_repeat(250,120)

    # Main loop.
    done = False
    game_start = True
    gamemode = None
    while not done:
        #if game not started, show menu
        if game_start:
            gamemode=menu.show_menu(console, screen, gamemode)
            game_start = False
            need_redraw = True
            help = False
            if gamemode is None:
                done = True
                pygame.display.quit()
                continue
            elif isinstance(gamemode,tuple):
                #load custom level
                endscreen = False
                victory = False
                game_mode=utils.gamemodes[gamemode[0]]
                tier = 0
                lvl = setup_game(game_mode, tier, gamemode[1])
                gamedata=gamemode[1]
                gamemode=gamemode[0]
            elif gamemode>=0:
                #setup game if gamemode selected
                endscreen = False
                victory = False
                game_mode=utils.gamemodes[gamemode]
                tier = 0
                lvl = setup_game(game_mode, tier)
                gamedata=None
                
        #main game loop
        actions = []
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            elif event.type == KEYDOWN:
                mods = pygame.key.get_mods()
                if (event.key == K_F4 and (mods & KMOD_ALT)) or (event.key == K_q and (mods & KMOD_CTRL)):
                    done = True
                if event.key == K_ESCAPE:
                    game_start = True
                    continue
                elif event.key == K_F1:
                    help = not help
                    need_redraw = True
                elif not (endscreen or event.key == K_BACKSPACE):
                    if event.key == K_TAB:
                        lvl.player.inventory.toggle()
                        need_redraw = True
                    else:
                        for a in utils.actions:
                            for key in a[1]:
                                if event.key == key:
                                    actions.append(a)
                elif event.key in (K_BACKSPACE, K_KP_ENTER, K_RETURN, K_SPACE):
                    # Endscreen - reset game
                    if victory:
                        old_inv=lvl.player.inventory #not deleting inventory if victory
                        tier+=1
                        if gamemode!=utils.gamemodes.index("Custom levels"): menu.save_score(game_mode, tier) #saving highscore as it's reached
                        else: menu.save_score(gamedata['selected_levels'], tier)
                        lvl = setup_game(game_mode, tier, gamedata)
                        if lvl is None:
                            if gamemode==utils.gamemodes.index("Custom levels"): menu.save_score(gamedata['selected_levels'], float(tier))
                            gamemode=None
                            game_start=True
                            break
                        lvl.player.inventory=old_inv
                        endscreen = False
                        victory = False
                        console.clear()
                        need_redraw = True
                    else:
                        game_start = True
                        gamemode = utils.gamemodes.index(game_mode)
                        need_redraw = False

        
        if lvl is not None and lvl.tick(actions):
            need_redraw = True
            if lvl.player.remove:
                victory = lvl.player.victory
                endscreen = True
        
        if effects2.animator.need_redraw:
            need_redraw=True
            effects2.animator.need_redraw=False
   
        if need_redraw:
            need_redraw = False
            console.clear()
            lvl.render(console)
            lvl.tint.render(console)
            lvl.player.inventory.render(console)

            if endscreen and not victory:
                console.fg_color = pygame.Color(200, 200, 200)
                console.print(35, 11, "--------------")
                console.print(35, 12, "| Game over. |")
                console.print(35, 13, "--------------")

            elif endscreen and victory:
                console.fg_color = pygame.Color(200, 200, 200)
                console.print(35, 11, "--------------")
                console.print(35, 12, "|  Victory!  |")
                console.print(35, 13, "--------------")
                

            screen.fill(pygame.Color(0,0,0))
            if help: menu.show_help(console, screen, True)
            console.render(screen)
        pygame.display.flip()
    pygame.display.quit()

if __name__ == "__main__":
    main()
                
