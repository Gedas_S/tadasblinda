""" For managing level progression """
import utils

def setup(tier):
	# default object amounts in a level
	counts = {'number_guards':3,
	          'number_boulders':2,
	          'number_pots':3,
	          'number_spiders':6,
	          'number_kittens':1,
	          'number_smoke_bug':3,
	          'number_power_source':5,
	          'number_snake':1,
	          'number_items':4,
	          'number_crossbow_guards':1,
	          'exit_difficulty':15,
	          'give_sapling':1
	          }
	if tier:
		for key in counts.keys():
			counts[key] = 0
		if tier == 2:
			counts['number_guards'] = 1
		elif tier == 3:
			counts['number_power_source'] = 1
			counts['exit_difficulty'] = 1
		elif tier == 4:
			counts['number_guards'] = 1
			counts['number_power_source'] = 3
			counts['exit_difficulty'] = 6
		elif tier == 5:
			counts['number_guards'] = 1
			counts['number_power_source'] = 3
			counts['exit_difficulty'] = 6		
			counts['number_items'] = 4
		elif tier == 6:
			counts['number_guards'] = 4
			counts['number_power_source'] = 2
			counts['exit_difficulty'] = 4
			counts['number_pots'] = 5
		elif tier == 7:
			counts['number_guards'] = 1
			counts['number_power_source'] = 4
			counts['exit_difficulty'] = 6		
			counts['number_spiders'] = 6
			counts['give_sapling'] = 1
		elif tier == 7:
			counts['number_guards'] = 1
			counts['number_power_source'] = 4
			counts['exit_difficulty'] = 6		
			counts['number_spiders'] = 6
			counts['give_sapling'] = 1
		elif tier == 8:
			counts['number_guards'] = 4
			counts['number_power_source'] = 4
			counts['exit_difficulty'] = 6		
			counts['number_spiders'] = 2
			counts['give_sapling'] = 1
			counts['number_boulders'] = 5
		elif tier == 9:
			counts['number_guards'] = 2
			counts['number_snake'] = 6
			counts['exit_difficulty'] = 7		
			counts['number_spiders'] = 2
			counts['give_sapling'] = 1
			counts['number_items'] = 5
			counts['number_smoke_bug'] = 4
		elif tier == 10:
			counts['number_crossbow_guards'] = 4
			counts['number_power_source'] = 6
			counts['exit_difficulty'] = 10		
			counts['number_spiders'] = 2
			counts['give_sapling'] = 1
			counts['number_items'] = 6
			counts['number_smoke_bug'] = 2
		elif tier > 10:
			counts['number_guards'] = tier // 2 - tier // 4
			counts['number_boulders'] = tier // 4
			counts['number_pots'] = (tier // 3) % 3
			counts['number_spiders'] = 2 + tier // 4
			counts['number_kittens'] = tier // 5
			if tier // 5 > 2:counts['number_kittens'] = 2
			counts['number_smoke_bug'] = 0
			if tier > 1:
				counts['number_smoke_bug'] = 1 + tier // 5
			counts['number_power_source'] = tier // 4
			if 10 - tier // 2 < 5: counts['number_power_source'] = 5
			counts['number_snake'] = 0
			if tier > 6: counts['number_snake'] = 1
			counts['number_items'] = tier // 2
			counts['number_crossbow_guards'] = tier // 5
			counts['exit_difficulty'] = counts['number_power_source'] * (tier // 10 + 1)
			counts['give_sapling'] = 1

	return counts

def setup_campaign(tier):
    return setup(tier+11)
    
def setup_tutorial(tier):
    return setup(tier+1)
    
def setup_classic(tier):
    return {'number_guards':3,
            'number_boulders':2,
            'number_pots':3,
            'number_spiders':6,
            'number_kittens':1,
            'number_smoke_bug':3,
            'number_power_source':5,
            'number_snake':1,
            'number_items':4,
            'number_crossbow_guards':1,
            'exit_difficulty':15,
            'give_sapling':1
            }
            
def setup_minimalist(tier):
    return {'number_guards':0,
            'number_boulders':0,
            'number_pots':0,
            'number_spiders':0,
            'number_kittens':0,
            'number_smoke_bug':0,
            'number_power_source':0,
            'number_snake':0,
            'number_items':0,
            'number_crossbow_guards':0,
            'exit_difficulty':None,
            'give_sapling':0
            }
            
level_setup = dict(zip(utils.gamemodes,(setup_tutorial, setup_campaign, setup_classic, setup_minimalist)))
