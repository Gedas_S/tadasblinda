import entity
import inventory, traps, guard
import utils, effects2
import pygame

class Player(entity.Entity):
    def __init__(self, x, y):
        super(Player, self).__init__(x, y)
        self.char = '@'
        self.hitting = True
        self.inventory = inventory.Inventory()
        self.invisible = 0
        self.power_charge = 0
        self.victory = False
        self.hidden = False
        self.solid = False

    def tick(self, room, actions):
        action_done = False
        for action in actions:
            if action[0] in list(range(4)):
                b = 1
                dx, dy = utils.offsets4[action[0]]
                nx, ny = self.x+dx*b, self.y+dy*b
                if room.is_walkable(nx, ny):
                    self.x, self.y = nx, ny
                    action_done = True
            elif action[0] in list(utils.item_letters):
                action_done = self.inventory.use_item(self, action[0], room)
            elif action[0] == 999:
                action_done = True
                #If hiding in a pot, get hiding status and become invisible. Moved from pot logic so status is always obtained before guard movement.
                for obj in room.objects[(self.x,self.y)]:
                    if isinstance(obj,traps.Pot) and obj.open>2:
                        self.hidden = True
                        self.hitting = False
                        self.invisible = max(self.invisible, 2)
            elif action[0] == 'l':
                effects2.animator.lightning(self.x, self.y, room) #call lightning
                action_done = True
            #remove hiding status if pot left
            if action[0] != 999 and self.hidden:
                self.hidden = False
                self.hitting = True
        if action_done:
            if self.invisible:
                self.invisible -= 1
                if self.invisible: #everyone else, including guards, act AFTER the player, so they will see him this turn already.
                    room.tint.bg[self.x][self.y]=pygame.Color(100,100,100)
                    room.tint.fade[self.x][self.y]=0.9
            return True
        return False
    
    def hit(self, room, obj):
        if isinstance(obj, traps.Boulder) or not self.hidden:
            self.remove = True
            death_color = pygame.Color(255,55,55)
            if self.victory: pass
            elif isinstance(obj, traps.Boulder):
                self.inventory.set_message("A huge boulder crushes you.", color=death_color)
            elif isinstance(obj, traps.PlaceableTrap):
                self.inventory.set_message("You stepped into your own trap.", color=death_color)
            elif isinstance(obj, guard.Guard):
                self.inventory.set_message("A guard quickly dispatched you.", color=death_color)
            elif isinstance(obj, traps.Crossbow_Bolt):
                self.inventory.set_message("You were shot with a crossbow bolt.", color=death_color)
            elif isinstance(obj, effects2.Blink_Effect):
                self.inventory.set_message("You teleported into something solid.", color=death_color)
            
    

class RoomExit(entity.Entity):
    def __init__(self, x, y):
        super(RoomExit, self).__init__(x, y)
        self.color = pygame.Color(100, 100, 190)
        self.char = '>'
        self.solid = False

    def hit(self,room,obj):
        if isinstance(obj, Player):
            obj.victory=True
            obj.remove=True
